#!/bin/bash

sudo timedatectl set-ntp true
sudo hwclock --systohc

sudo reflector -c Italy -a 6 --sort rate --save /etc/pacman.d/mirrorlist

git clone https://aur.archlinux.org/pikaur.git
cd pikaur/
makepkg -si --noconfirm

pikaur -S --noconfirm topgrade spotify zsh-theme-powerlevel10k-git
echo 'source /usr/share/zsh-theme-powerlevel10k/powerlevel10k.zsh-theme' >> ~/.zshrc

sudo pacman -S --noconfirm xorg sddm plasma kde-graphics-meta kde-system-meta kde-utilities-meta kdeconnect kamoso kmix krdc knotes krfb kdenetwork-filesharing firefox mpv nnn neofetch htop ncdu wget curl bittorrent telegram-desktop bitwarden

sudo systemctl enable sddm
/bin/echo -e "\e[1;32mREBOOTING IN 5..4..3..2..1..\e[0m"
sleep 5
reboot
