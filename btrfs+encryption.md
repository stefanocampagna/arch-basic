# Arch BTRF + Encryption commands

gdisk /dev/sda

-- efi partition
n 
default
default
+300M
EF000

-- swap partition
n
default
default
+1G
8200

-- root partition
n
default
default
default
default

-- write changes
w


-- formatting
mkfs.fat -F32 /dev/sda1
mkswap /dev/sda2 
swapon /dev/sda2

-- creating encrypted root
cryptsetup luksFormat /dev/sda3
cryptsetup luksOpen /dev/sda3 cryptroot
mkfs.btrfs /dev/mapper/cryptroot

-- mount partition for subvolume creation
mount /dev/mapper/cryptroot /mnt
cd /mnt
btrfs subvolume create @ 
btrfs subvolume create @home
cd
umount /mnt

-- remount
mount -o noatime,compress=zstd,space_cache,discard=async,subvol=@ /dev/mapper/cryptroot /mnt
mkdir /mnt/home
mount -o noatime,compress=zstd,space_cache,discard=async,subvol=@home /dev/mapper/cryptroot /mnt/home
mkdir /mnt/boot/efi
mount /dev/sda1 /mnt/boot/efi

pacstrap /mnt base linux linux-firmware git vim intel-ucode btrfs-progs

genfstab -U /mnt >> /mnt/etc/fstab

arch-chroot /mnt
git clone https://gitlab.com/stefanocampagna/arch-basic
cd arch-basic
chmod +x ./base-uefi.sh
